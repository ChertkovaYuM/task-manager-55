package ru.tsc.chertkova.tm.exception.user;

import org.jetbrains.annotations.NotNull;
import ru.tsc.chertkova.tm.exception.AbstractException;

public final class AccessDeniedException extends AbstractException {

    public AccessDeniedException() {
        super("Error! Access denied...");
    }

    public AccessDeniedException(@NotNull Throwable cause) {
        super(cause);
    }

}
