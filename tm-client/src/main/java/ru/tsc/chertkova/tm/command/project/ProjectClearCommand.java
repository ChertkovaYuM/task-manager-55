package ru.tsc.chertkova.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Component;
import ru.tsc.chertkova.tm.dto.request.project.ProjectClearRequest;

@Component
public final class ProjectClearCommand extends AbstractProjectCommand {

    @NotNull
    public static final String NAME = "project-clear";

    @NotNull
    public static final String DESCRIPTION = "Remove all projects.";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        System.out.println("[PROJECT CLEAR]");
        projectEndpoint.clearProject(new ProjectClearRequest(getToken()));
    }

}
