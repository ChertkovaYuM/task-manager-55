package ru.tsc.chertkova.tm.command.domain;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Component;
import ru.tsc.chertkova.tm.dto.request.data.DataBase64SaveRequest;
import ru.tsc.chertkova.tm.enumerated.Role;

@Component
public final class DomainBase64SaveCommand extends AbstractDomainCommand {

    @NotNull
    public static final String NAME = "data-save-base64";

    @NotNull
    public static final String DESCRIPTION = "Save date in base64 file.";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    @Override
    @SneakyThrows
    public void execute() {
        System.out.println("[DATA SAVE BASE64]");
        getDomainEndpoint().saveDataBase64(new DataBase64SaveRequest(getToken()));
    }

    @Nullable
    @Override
    public Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }

}
