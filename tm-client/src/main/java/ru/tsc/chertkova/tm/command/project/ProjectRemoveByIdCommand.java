package ru.tsc.chertkova.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Component;
import ru.tsc.chertkova.tm.dto.request.project.ProjectRemoveByIdRequest;
import ru.tsc.chertkova.tm.model.Project;
import ru.tsc.chertkova.tm.util.TerminalUtil;

@Component
public final class ProjectRemoveByIdCommand extends AbstractProjectCommand {

    @NotNull
    public static final String NAME = "project-remove-by-id";

    @NotNull
    public static final String DESCRIPTION = "Remove project by id.";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        System.out.println("[REMOVE PROJECT BY ID]");
        System.out.println("ENTER ID:");
        @Nullable final String id = TerminalUtil.nextLine();
        projectEndpoint.removeProjectById(new ProjectRemoveByIdRequest(getToken(), id));
    }

}
