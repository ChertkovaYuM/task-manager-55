package ru.tsc.chertkova.tm.command.domain;

import lombok.Getter;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.tsc.chertkova.tm.api.endpoint.IDomainEndpoint;
import ru.tsc.chertkova.tm.command.AbstractCommand;
import ru.tsc.chertkova.tm.dto.Domain;

@Getter
@Component
@NoArgsConstructor
public abstract class AbstractDomainCommand extends AbstractCommand {

    @Autowired
    private IDomainEndpoint domainEndpoint;

}
