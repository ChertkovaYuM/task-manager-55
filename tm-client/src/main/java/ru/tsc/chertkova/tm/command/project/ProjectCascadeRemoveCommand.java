package ru.tsc.chertkova.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Component;
import ru.tsc.chertkova.tm.dto.request.project.ProjectRemoveByIdRequest;
import ru.tsc.chertkova.tm.util.TerminalUtil;

@Component
public final class ProjectCascadeRemoveCommand extends AbstractProjectCommand {

    @NotNull
    public static final String NAME = "project-cascade-remove";

    @NotNull
    public static final String DESCRIPTION = "Project cascade remove.";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        System.out.println("[CASCADE REMOVE PROJECT]");
        System.out.println("ENTER PROJECT ID:");
        @Nullable final String projectId = TerminalUtil.nextLine();
        projectEndpoint.removeProjectById(new ProjectRemoveByIdRequest(getToken(), projectId));
    }

}
