package ru.tsc.chertkova.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.tsc.chertkova.tm.api.endpoint.ITaskEndpoint;
import ru.tsc.chertkova.tm.dto.request.task.TaskUnbindToProjectRequest;
import ru.tsc.chertkova.tm.util.TerminalUtil;

@Component
public final class TaskUnbindToProjectCommand extends AbstractProjectCommand {

    @Nullable
    @Autowired
    protected ITaskEndpoint taskEndpoint;

    @NotNull
    public static final String NAME = "unbind-task-from-project";

    @NotNull
    public static final String DESCRIPTION = "Unbind task from project.";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        System.out.println("[UNBIND TASK TO PROJECT]");
        System.out.println("ENTER PROJECT ID:");
        @Nullable final String projectId = TerminalUtil.nextLine();
        System.out.println("ENTER TASK ID:");
        @Nullable final String taskId = TerminalUtil.nextLine();
        taskEndpoint.unbindTaskToProject(new TaskUnbindToProjectRequest(getToken(), projectId, taskId));
    }

}
